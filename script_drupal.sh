#!/bin/bash

# sudo su
# sh script_drupal.sh hola.com 
# faltarien echo's per informar com va la cosa... i també treure l'output si no hi han errors...

if [ $# -ne 1 ] 
then
    echo "Invalid number of arguments please pass only one argument, the hostname. For example: hola.com"
    exit 1
fi

hostname=$1

printInfo () {
	echo "\n\n\n\n"
	echo "$1"
	echo "\n\n\n\n"
}




apt update

apt install -y apache2 # instala servidor web

apt install -y php # instala interpret php

apt install -y libapache2-mod-php # instala modul d'apache que serveix per interectuar amb php

apt install -y mysql-server # instala sistema gestor de bbdd mysql

apt install -y php-mysql # instala moduls de php necessaris

apt install -y php-xml

apt install -y php-gd

apt install -y wget

apt install -y systemctl

a2enmod rewrite # activa modul de apache (ve instalat per defecte)






printInfo "-- DOWNLOADING DRUPAL --" 

wget https://www.drupal.org/files/projects/drupal-9.3.2.tar.gz # descarreguem el codi
mv drupal-9.3.2.tar.gz /var/www/
cd /var/www
tar -zxf drupal-9.3.2.tar.gz 
mv drupal-9.3.2 $hostname


# (TOT LO DE DINS ES SEU (de root user) DE COP... D'ELL I DEL SEU GRUP) també podria posar l'usuari de apache al grup de root...
# pues no cal.
# chown -R www-data:www-data /var/www/$hostname 
# Tampoc cal





printInfo "-- EDITING APACHE FILES --"

touch /etc/apache2/sites-available/$hostname.conf

cat >> /etc/apache2/sites-available/$hostname.conf << EOF

<VirtualHost *:80>
    
    <Directory /var/www/$hostname>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
    
    ServerName $hostname
    ServerAlias www.$hostname
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/$hostname
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

EOF

a2ensite $hostname

# systemctl reload apache2 # no cal si fas restart...

systemctl restart apache2

systemctl start mysql






printInfo "-- DOWNLOADING COMPOSER --"

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" # downloading composer...
php composer-setup.php

COMPOSER_ALLOW_SUPERUSER=1
export COMPOSER_ALLOW_SUPERUSER 





printInfo "-- INSTALLING DRUPAL--"

cd $hostname
php ../composer.phar install --no-interaction # installing drupal...

# For additional security you should declare the allow-plugins config with a list of packages names that are allowed to run code. 
# See https://getcomposer.org/allow-plugins






printInfo "-- DRUPAL SETUP --"

mkdir sites/default/files
chmod a+w sites/default/files

cp sites/default/default.settings.php sites/default/settings.php
chmod a+w sites/default/settings.php






printInfo "-- SQL SETUP --"

cd 

touch script_mysql.sql # Creant les bbdd i usuaris necessaris...

cat >> script_mysql.sql << EOF

CREATE USER IF NOT EXISTS 'drupal_user'@'localhost' IDENTIFIED BY 'drupal_1234';

CREATE DATABASE IF NOT EXISTS drupal_db; 

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON drupal_db.* TO 'drupal_user'@'localhost';

EOF

mysql -u root < script_mysql.sql




printInfo "-- ALL DONE SUCCESSFULLY --"




